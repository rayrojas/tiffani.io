import wx
import json

import UI.core.ui.ribbon as RB

from UI.core.ui.octopus import addtoPanel
from UI.core.ui.settings import CONSTANTS
#from burocrata import Repository
from UI.search import Ui_search
from UI.new import Ui_new
from UI.supplier import Ui_supplier
from UI.composition import Ui_composition
from UI.output_register import Ui_output_register
from UI.transactions import Ui_transactions
from UI.input_register import Ui_input_register
from UI.lote import Ui_lote
from textin import Texton

elements = {
	'buttons' : {}
}

elements['buttons']['search'] = wx.ID_HIGHEST + 1
elements['buttons']['register'] = elements['buttons']['search'] + 1
elements['buttons']['report'] = elements['buttons']['register'] + 1
elements['buttons']['supplier'] = elements['buttons']['report'] + 1
elements['buttons']['close'] = elements['buttons']['supplier'] + 1
elements['buttons']['composition'] = elements['buttons']['close'] + 1



def CreateBitmap(xpm):

	#bmp = wx.Bitmap("bitmaps/%s"%xpm, wx.BITMAP_TYPE_XPM)
	bmp = wx.Image("res/bitmaps/%s"%xpm, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
	
	return bmp


class ColourClientData(object):

	def __init__(self, name, colour):
		
		self._name = name
		self._colour = colour


	def GetName(self):

		return self._name

	
	def GetColour(self):

		return self._colour



class RibbonFrame(wx.Frame):

	def __init__(self, parent, id=wx.ID_ANY, title="", pos=wx.DefaultPosition,
				 size=wx.DefaultSize, style=wx.DEFAULT_FRAME_STYLE):

		wx.Frame.__init__(self, parent, id, title, pos, size, style)
		# Configurar Frame
		self.SetBackgroundColour(CONSTANTS['palete']['normal'])

		# Ribbon
		self._ribbon = RB.RibbonBar(self, wx.ID_ANY)
		home = RB.RibbonPage(self._ribbon, wx.ID_ANY, "Inicio", CreateBitmap("ribbon.xpm"))
		addtoPanel(
			RB.RibbonPanel(home, wx.ID_ANY, "Herramientas", CreateBitmap("circle_small.xpm")),
			[
				{
					'button' : elements['buttons']['search'],
					'text' : "Busqueda",
					'bitmap' : CreateBitmap("tiffani.io.search.png"),
					'other_text' : CreateBitmap("circle_small.xpm")
				},
				{
					'button' : elements['buttons']['register'],
					'text' : "Registrar",
					'bitmap' : CreateBitmap("tiffani.io.register.png"),
					'other_text' : ""
				},
				{
					'button' : elements['buttons']['register'],
					'text' : "Tipos",
					'bitmap' : CreateBitmap("tiffani.io.kind.png"),
					'other_text' : ""
				},				
				{
					'button' : elements['buttons']['supplier'],
					'text' : "Proveedores",
					'bitmap' : CreateBitmap("tiffani.io.supplier.png"),
					'other_text' : ""
				},
				{
					'button' : elements['buttons']['composition'],
					'text' : "Composiciones",
					'bitmap' : CreateBitmap("tiffani.io.composition.png"),
					'other_text' : ""
				},
				{
					'button' : elements['buttons']['report'],
					'text' : "Reportes",
					'bitmap' : CreateBitmap("tiffani.io.reports.png"),
					'other_text' : ""
				},
				{
					'button' : elements['buttons']['close'],
					'text' : "Cerrar",
					'bitmap' : CreateBitmap("tiffani.io.reports.png"),
					'other_text' : ""
				},
			]
		)
		self._ribbon.Realize()

		#Paneles
		self._paneles = {}

		#Layout
		self.MainSizer = wx.BoxSizer(wx.VERTICAL)
		self.RibbonSizer = wx.BoxSizer(wx.VERTICAL)
		self.ContentSizer = wx.BoxSizer(wx.HORIZONTAL)
		self.RibbonSizer.Add(self._ribbon, 0, wx.EXPAND)
		self.MainSizer.Add(self.RibbonSizer, 0, wx.EXPAND)
		self.MainSizer.Add(self.ContentSizer, 1, wx.EXPAND)
		self.SetSizer(self.MainSizer)
		self.BindEvents()

	def show_panel_search(self):
		self._paneles['panel_search'] = wx.Panel(self, wx.ID_ANY, style = CONSTANTS['border'])
		self._paneles['panel_search'].SetBackgroundColour(CONSTANTS['palete']['normal'])
		self.ContentSizer.Add(self._paneles['panel_search'], 1, wx.TOP|wx.LEFT|wx.RIGHT|wx.EXPAND, 10)
		Ui_search(self._paneles['panel_search'], self)
		self.hide_paneles('panel_search')
		self.Layout()

	def show_panel_input_register(self, data):
		self._paneles['panel_input_register'] = wx.Panel(self, wx.ID_ANY, style = CONSTANTS['border'])
		self._paneles['panel_input_register'].SetBackgroundColour(CONSTANTS['palete']['normal'])
		self.ContentSizer.Add(self._paneles['panel_input_register'], 1, wx.TOP|wx.LEFT|wx.RIGHT|wx.EXPAND, 10)
		Ui_input_register(self._paneles['panel_input_register'], self, data)
		self.hide_paneles('panel_input_register')
		self.Layout()

	def show_panel_output_register(self, data):
		self._paneles['panel_output_register'] = wx.Panel(self, wx.ID_ANY, style = CONSTANTS['border'])
		self._paneles['panel_output_register'].SetBackgroundColour(CONSTANTS['palete']['normal'])
		self.ContentSizer.Add(self._paneles['panel_output_register'], 1, wx.TOP|wx.LEFT|wx.RIGHT|wx.EXPAND, 10)
		Ui_output_register(self._paneles['panel_output_register'], self, data)
		self.hide_paneles('panel_output_register')
		self.Layout()

	def show_panel_transactions(self, data):
		self._paneles['panel_transactions'] = wx.Panel(self, wx.ID_ANY, style = CONSTANTS['border'])
		self._paneles['panel_transactions'].SetBackgroundColour(CONSTANTS['palete']['normal'])
		self.ContentSizer.Add(self._paneles['panel_transactions'], 1, wx.TOP|wx.LEFT|wx.RIGHT|wx.EXPAND, 10)
		Ui_transactions(self._paneles['panel_transactions'], self, data)
		self.hide_paneles('panel_transactions')
		self.Layout()

	def show_panel_lote(self, data):
		self._paneles['panel_lote'] = wx.Panel(self, wx.ID_ANY, style = CONSTANTS['border'])
		self._paneles['panel_lote'].SetBackgroundColour(CONSTANTS['palete']['normal'])
		self.ContentSizer.Add(self._paneles['panel_lote'], 1, wx.TOP|wx.LEFT|wx.RIGHT|wx.EXPAND, 10)
		Ui_lote(self._paneles['panel_lote'], self, data)
		self.hide_paneles('panel_lote')
		self.Layout()		

	def show_panel_supplier(self):
		if 'panel_supplier' in self._paneles:
			self.hide_paneles('panel_supplier')
			self._paneles['panel_supplier'].Show()
		else:
			self._paneles['panel_supplier'] = wx.Panel(self, wx.ID_ANY, style = CONSTANTS['border'])
			self._paneles['panel_supplier'].SetBackgroundColour(CONSTANTS['palete']['normal'])
			self.ContentSizer.Add(self._paneles['panel_supplier'], 1, wx.TOP|wx.LEFT|wx.RIGHT|wx.EXPAND, 10)
			Ui_supplier(self._paneles['panel_supplier'], self)
			self.hide_paneles('panel_supplier')
		self.Layout()

	def show_panel_new(self):
		if 'panel_register' in self._paneles:
			self.hide_paneles('panel_register')
			self._paneles['panel_register'].Show()
		else:
			self._paneles['panel_register'] = wx.Panel(self, wx.ID_ANY, style = CONSTANTS['border'])
			self._paneles['panel_register'].SetBackgroundColour(CONSTANTS['palete']['normal'])
			self.ContentSizer.Add(self._paneles['panel_register'], 1, wx.TOP|wx.LEFT|wx.RIGHT|wx.EXPAND, 10)
			Ui_new(self._paneles['panel_register'], self)
			self.hide_paneles('panel_register')
		self.Layout()

	def show_panel_composition(self):
		if 'panel_composition' in self._paneles:
			self.hide_paneles('panel_composition')
			self._paneles['panel_composition'].Show()
		else:
			self._paneles['panel_composition'] = wx.Panel(self, wx.ID_ANY, style = CONSTANTS['border'])
			self._paneles['panel_composition'].SetBackgroundColour(CONSTANTS['palete']['normal'])
			self.ContentSizer.Add(self._paneles['panel_composition'], 1, wx.TOP|wx.LEFT|wx.RIGHT|wx.EXPAND, 10)
			Ui_composition(self._paneles['panel_composition'], self)
			self.hide_paneles('panel_composition')
		self.Layout()

	def hide_paneles(self, exception = ''):
		if exception in self._paneles:
			for x in self._paneles:
				if x != exception:
					self._paneles[x].Hide()
		else:
			for x in self._paneles:
				self._paneles[x].Hide()

	def BindEvents(self):
		self.Bind(RB.EVT_RIBBONBUTTONBAR_CLICKED, self.On_panel_search, id=elements['buttons']['search'])
		self.Bind(RB.EVT_RIBBONBUTTONBAR_CLICKED, self.On_panel_register, id=elements['buttons']['register'])
		self.Bind(RB.EVT_RIBBONBUTTONBAR_CLICKED, self.On_panel_supplier, id=elements['buttons']['supplier'])
		self.Bind(RB.EVT_RIBBONBUTTONBAR_CLICKED, self.On_panel_composition, id=elements['buttons']['composition'])


	def On_panel_search(self, event):
		self.show_panel_search()

	def On_panel_register(self, event):
		self.show_panel_new()

	def On_panel_supplier(self, event):
		self.show_panel_supplier()

	def On_panel_composition(self, event):
		self.show_panel_composition()

if __name__ == "__main__":

	app = wx.PySimpleApp()
	frame = RibbonFrame(None, -1, "Classic Alpaca - Almacenes", size=(800, 600))
	frame.CenterOnScreen()
	frame.Show()
	app.MainLoop()