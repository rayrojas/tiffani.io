import ribbon as RB

def addtoPanel(p, params):
	r = RB.RibbonButtonBar(p)
	for x in params:
		r.AddSimpleButton(x['button'], x['text'], x['bitmap'], x['other_text'])
	return True