import wx

CONSTANTS = {
	#'border' : wx.SIMPLE_BORDER,
	'border' : wx.NO_BORDER,
	#'source' : 'http://admin.alpaca.webfactional.com/api/core/',
	'source' : 'http://127.0.0.1:8000/api/core/',
	'palete' : {
		'black' : '#333',
		'light' : '#8fb8e8',
		'normal' : '#C0D7F2',
		'weight' : '#8CB2E2',
		'dark' : '#244e8f',
		'red' : '#FF6069',
		'white' : '#fff'
	},
	'resources' : 'res/bitmaps'
	#'font' : {
	#	'8' : wx.Font(8, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL),
	#	'12' : wx.Font(12, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL),
	#	'14' : wx.Font(14, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL)
	#}
}