import sys
import wx
from wx.lib.mixins.listctrl import ListCtrlAutoWidthMixin

from settings import CONSTANTS

class AutoWidthListCtrl(wx.ListCtrl, ListCtrlAutoWidthMixin):
	def __init__(self, parent):
		wx.ListCtrl.__init__(self, parent, wx.ID_ANY, style = wx.LC_REPORT)
		ListCtrlAutoWidthMixin.__init__(self)

class Ui():
	def __init__(self, panel, parent, data = {}):
		self._panels, self._sizers, self._elements, self._parent, self._s_sizer, self._data = {}, {}, {}, parent, {}, data;
		
		#Sizers
		self._sizers['main'] = wx.BoxSizer(wx.VERTICAL)
		self._sizers['head'] = wx.BoxSizer(wx.VERTICAL)
		self._sizers['content'] = wx.BoxSizer(wx.VERTICAL)
		########################################################################
		################### PANEL
		########################################################################
		self._panels['root'] = panel
		self._panels['head'] = wx.Panel(self._panels['root'], -1, style = CONSTANTS['border'])
		self._panels['head'].SetBackgroundColour(CONSTANTS['palete']['normal'])
		self._panels['head'].SetForegroundColour(CONSTANTS['palete']['light'])
		self._panels['head'].SetFont(wx.Font(24, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL))
		########################################################################
		################### CONTENT
		########################################################################
		self._panels['content'] = wx.Panel(self._panels['root'], -1, style = CONSTANTS['border'])
		self._panels['content'].SetBackgroundColour(CONSTANTS['palete']['normal'])
		self._panels['content'].SetForegroundColour(CONSTANTS['palete']['dark'])

		self._render()

	def _render(self):
		return True

	def _create_bitmap(self, resource):
		return wx.Image("%s/%s"%(CONSTANTS['resources'], resource), wx.BITMAP_TYPE_ANY).ConvertToBitmap()

	def _render_head(self):
		'''
			Renderiza head con icono y texto incluido
		'''
		tmp_z = wx.BoxSizer(wx.HORIZONTAL)
		tmp_icon = self._create_bitmap(self._constants['icon'])
		tmp_z.Add(wx.StaticBitmap(self._panels['head'], wx.ID_ANY, tmp_icon))
		tmp_z.Add(wx.StaticText(self._panels['head'], wx.ID_ANY, self._constants['title']), 1, wx.EXPAND)
		self._panels['head'].SetSizer(tmp_z)
		self._sizers['head'].Add(self._panels['head'], 1, wx.EXPAND)
		self._sizers['main'].Add(self._sizers['head'], 0, wx.EXPAND)

	def _render_split(self, sizer = 'main', weight = 20):
		self._sizers[sizer].Add((-1, weight))

	def _render_split_line(self, sizer = 'main', parent = 'root', color = CONSTANTS['palete']['normal']):
		'''
			Renderiza linea que separa dos areas, ejemplo: head y content
		'''		
		line = wx.StaticLine(self._panels[parent])
		line.SetBackgroundColour(color)
		self._sizers[sizer].Add(line, 0, wx.ALL|wx.EXPAND, 5)	

	def _render_content(self):
		'''
			Inserta el content dentro del main
		'''
		self._sizers['content'].Add(self._panels['content'], 1, wx.EXPAND)
		self._sizers['main'].Add(self._sizers['content'], 1, wx.EXPAND)

	def _render_content_layout_part(self, panel, parent, s_b_z, s_z, text = 'Undefined', orient = wx.VERTICAL):
		self._panels[panel] = wx.Panel(self._panels[parent], wx.ID_ANY, style = CONSTANTS['border'])
		self._panels[panel].SetBackgroundColour(CONSTANTS['palete']['normal'])
		
		_tmp_s_sizer = wx.StaticBox(self._panels[panel], wx.ID_ANY, text)
		_tmp_s_sizer.SetForegroundColour(CONSTANTS['palete']['dark'])
		_tmp_s_sizer.SetBackgroundColour(CONSTANTS['palete']['normal'])
		_tmp_s_sizer.SetFont(wx.Font(9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL))
		
		self._elements[s_b_z] = wx.StaticBoxSizer(_tmp_s_sizer, wx.VERTICAL)

		self._panels[panel].SetSizer(self._elements[s_b_z])
		self._sizers[s_z].Add(self._panels[panel], 1, wx.EXPAND|wx.ALL, 5)


	def _render_prepare_layout(self):
		self._sizers['content'].Add(self._panels['content'], 1, wx.EXPAND)
		self._sizers['main'].Add(self._sizers['content'], 1, wx.EXPAND)
		self._sizers['s-c-wrapper'] = wx.BoxSizer(wx.HORIZONTAL)

	def _render_content_layout(self, layout, show_layout):
		for i in range(len(layout)):
			if type(layout[i]['layout']) is dict:
				layout[i]['panel'] = '%s-p-c-l-%s' % (layout[i]['parent'], i)
				layout[i]['s_b_z'] = '%s-s-sb-c-l-%s' % (layout[i]['parent'], i)
				if show_layout:
					print "Panel=>", layout[i]['panel']
					print "StaticBoxSizer=>", layout[i]['s_b_z']
				self._render_content_layout_part(layout[i]['panel'], layout[i]['parent'], layout[i]['s_b_z'], layout[i]['s_z'], layout[i]['layout']['text'], layout[i]['layout']['orient'])

			elif type(layout[i]['layout']) is list:
				grand_parent, grand_sizer = 'p-c-l-%s-t' % (i), 's-c-l-%s-t' % (i)

				self._panels[grand_parent] = wx.Panel(self._panels[layout[i]['parent']], wx.ID_ANY, style = CONSTANTS['border'])
				
				self._panels[grand_parent].SetForegroundColour(CONSTANTS['palete']['dark'])
				self._panels[grand_parent].SetBackgroundColour(CONSTANTS['palete']['normal'])

				self._sizers[grand_sizer] = wx.BoxSizer(wx.VERTICAL)

				self._panels[grand_parent].SetSizer(self._sizers[grand_sizer])

				for j in range(len(layout[i]['layout'])):

					self._render_content_layout([{
						'parent' : grand_parent,
						's_z' : grand_sizer,
						'layout' : {
							'text' : layout[i]['layout'][j]['text'],
							'orient' : wx.HORIZONTAL,
						}
					}], show_layout)

				self._sizers[layout[i]['s_z']].Add(self._panels[grand_parent], 1, wx.EXPAND)




	def _render__content_layout(self, orient = wx.VERTICAL, panels = ['Default']):
		'''
			Inserta el content dentro del main
		'''
		self._sizers['content'].Add(self._panels['content'], 1, wx.EXPAND)
		self._sizers['main'].Add(self._sizers['content'], 1, wx.EXPAND)
		self._sizers['s-c-wrapper'] = wx.BoxSizer(orient)
		for x in range(len(panels)):
			if type(panels[x]) is str:
				self._panels['p-c-l-%s' % x] = wx.Panel(self._panels['content'], wx.ID_ANY, style = CONSTANTS['border'])
				self._panels['p-c-l-%s' % x].SetBackgroundColour(CONSTANTS['palete']['normal'])
				self._sizers['s-c-w-l-%s' % x] = wx.BoxSizer(orient)
				self._panels['p-c-l-%s' % x].SetSizer(self._sizers['s-c-w-l-%s' % x])
				self._s_sizer['sb-c-l-%s' % x] = wx.StaticBox(self._panels['p-c-l-%s' % x], wx.ID_ANY, panels[x])
				self._s_sizer['sb-c-l-%s' % x].SetForegroundColour(CONSTANTS['palete']['dark'])
				self._s_sizer['sb-c-l-%s' % x].SetBackgroundColour(CONSTANTS['palete']['normal'])
				self._s_sizer['sb-c-l-%s' % x].SetFont(wx.Font(9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL))
				self._elements['s-sb-c-l-%s' % x] = wx.StaticBoxSizer(self._s_sizer['sb-c-l-%s' % x], wx.VERTICAL)
				self._panels['p-c-l-%s' % x].SetSizer(self._elements['s-sb-c-l-%s' % x])
				self._sizers['s-c-wrapper'].Add(self._panels['p-c-l-%s' % x], 1, wx.EXPAND|wx.ALL, 5)
			elif type(panels[x]) is list:
				for y in range(len(panels[x])):
					self._panels['p-c-l-%s' % y] = wx.Panel(self._panels['content'], wx.ID_ANY, style = CONSTANTS['border'])
					self._panels['p-c-l-%s' % y].SetBackgroundColour(CONSTANTS['palete']['normal'])
					self._sizers['s-c-w-l-%s' % y] = wx.BoxSizer(orient)
					self._panels['p-c-l-%s' % y].SetSizer(self._sizers['s-c-w-l-%s' % y])
					self._s_sizer['sb-c-l-%s' % y] = wx.StaticBox(self._panels['p-c-l-%s' % y], wx.ID_ANY, panels[x][y])
					self._s_sizer['sb-c-l-%s' % y].SetForegroundColour(CONSTANTS['palete']['dark'])
					self._s_sizer['sb-c-l-%s' % y].SetBackgroundColour(CONSTANTS['palete']['normal'])
					self._s_sizer['sb-c-l-%s' % y].SetFont(wx.Font(9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL))
					self._elements['s-sb-c-l-%s' % y] = wx.StaticBoxSizer(self._s_sizer['sb-c-l-%s' % y], wx.VERTICAL)
					self._panels['p-c-l-%s' % y].SetSizer(self._elements['s-sb-c-l-%s' % y])
					self._sizers['s-c-wrapper'].Add(self._panels['p-c-l-%s' % y], 1, wx.EXPAND|wx.ALL, 5)

	def _render_end(self):
		self._panels['root'].SetSizer(self._sizers['main'])

	'''
		Creates
	'''
	def _create_static_box(self, parent, name, text):
		self._s_sizer[name] = wx.StaticBox(self._panels[parent], wx.ID_ANY, text)
		self._s_sizer[name].SetForegroundColour(CONSTANTS['palete']['dark'])
		self._s_sizer[name].SetBackgroundColour(CONSTANTS['palete']['normal'])
		self._s_sizer[name].SetFont(wx.Font(9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL))
		self._elements['s-%s'%name] = wx.StaticBoxSizer(self._s_sizer[name], wx.VERTICAL)
		self._panels[parent].SetSizer(self._elements['s-%s'%name])


	def _create_grid_sizer(self, name):
		self._elements[name] = wx.GridSizer(rows=4, cols=2, hgap=5, vgap=5)

	def _create_input_text(self, parent, name, size = (140, -1)):
		self._elements[name] = wx.TextCtrl(self._panels[parent], wx.ID_ANY, '', size = size)

	def _create_button(self, parent, name, text, size = (140, -1)):
		self._elements[name] = wx.Button(self._panels[parent], wx.ID_ANY, text, size = size)

	def _create_text(self, parent, name, text):
		self._elements[name] = wx.StaticText(self._panels[parent], wx.ID_ANY, text)

	def _create_table(self, parent, name):
		self._elements[name] = AutoWidthListCtrl(self._panels[parent])

	def _create_sizer(self, name, style = ''):
		self._sizers[name] = wx.BoxSizer(style)

	def _create_panel(self, parent, name):
		self._panels[name] = wx.Panel(self._panels[parent], wx.ID_ANY, style = CONSTANTS['border'])
		self._panels[name].SetBackgroundColour(CONSTANTS['palete']['normal'])

	'''
		Insertions
	'''

	def _insert_column_to_table(self, data, table):
		if type(data) is list:
			for x in data:
				self._elements[table].InsertColumn(x['position'], x['name'], width = x['width'])
		elif type(data) is dict:
			self._elements[table].InsertColumn(data['position'], data['name'], width = data['width'])

	def _insert_element_to_sizer(self, element, sizer, type = 'normal', style = wx.EXPAND, border = 0):
		if type == 'normal':
			self._sizers[sizer].Add(self._elements[element], 0, style)
		elif type == 'expand':
			self._sizers[sizer].Add(self._elements[element], 1, style)

	def _insert_panel_to_sizer(self, panel, sizer, type = "normal", style = wx.EXPAND, border = 0):
		if type == 'normal':
			self._sizers[sizer].Add(self._panels[panel], 0, style, border)
		elif type == 'expand':
			self._sizers[sizer].Add(self._panels[panel], 1, style, border)

	def _insert_sizer_to_panel(self, sizer, panel):
		self._panels[panel].SetSizer(self._sizers[sizer])

	def _insert_grid_to_panel(self, grid, panel):
		self._panels[panel].SetSizer(self._elements[grid])

	'''
		Faster
	'''

	def _faster_create_table(self, parent, name, sizer = wx.VERTICAL, style = "normal", fit = wx.EXPAND):
		z = wx.BoxSizer(sizer)
		self._elements[name] = wx.ListCtrl(self._panels[parent], style = wx.LC_REPORT|wx.BORDER_SUNKEN)
		if style == "normal": z.Add(self._elements[name], 0, fit)
		elif style == "expand": z.Add(self._elements[name], 1, fit)
		self._panels[parent].SetSizer(z)

	def _faster_create_text(self, parent, text, sizer = wx.VERTICAL, style = "normal", fit = wx.EXPAND):
		z = wx.BoxSizer(sizer)
		text = wx.StaticText(self._panels[parent], wx.ID_ANY, text)
		text.SetFont(wx.Font(9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL))
		text.SetForegroundColour(CONSTANTS['palete']['dark'])		
		if style == "normal": z.Add(text, 0, fit)
		elif style == "expand": z.Add(text, 1, fit)
		self._panels[parent].SetSizer(z)

	def _faster_create_line(self, parent, sizer = wx.VERTICAL, style = "normal", fit = wx.EXPAND, orient = wx.LI_VERTICAL):
		z = wx.BoxSizer(sizer)
		line = wx.StaticLine(self._panels[parent], wx.ID_ANY, style = orient)
		line.SetBackgroundColour(CONSTANTS['palete']['normal'])
		if style == "normal":
			z.Add(line, 0, fit)
		elif style == "expand":
			z.Add(line, 1, fit)
		self._panels[parent].SetSizer(z)

	def _addSizer(self):
		return True

	def _addPanel(self):
		return True

class Ui_table():
	def __init__(self, panel, parent, style = "normal", fit = wx.EXPAND, join = False):
		self._panels, self._sizers, self._elements, self._parent = {}, {}, {}, parent;
		self._panels['root'] = panel
		self._elements['table'] = wx.ListCtrl(self._panels['root'], style = wx.LC_REPORT|wx.BORDER_SUNKEN)
		z = wx.BoxSizer(wx.VERTICAL)
		if style == "normal": z.Add(self._elements['table'], 0, fit)
		elif style == "expand": z.Add(self._elements['table'], 1, fit)
		self._panels['root'].SetSizer(z)

		#self._elements['table'].itemDataMap = 
		#listmix.ColumnSorterMixin.__init__(self, 3)
		#self._parent.Bind(wx.EVT_LIST_COL_CLICK, self.OnClickColum, self._elements['table'])

	def _set_columns(self, data):
		if type(data) is list:
			for x in data:
				self._elements['table'].InsertColumn(x['position'], x['name'], width = x['width'])
		elif type(data) is dict:
			self._elements['table'].InsertColumn(data['position'], data['name'], width = data['width'])

	def _set_data(self, structure, data):
		if type(data) is dict:
			data = [data]
		for x in data:
			index = self._elements['table'].InsertStringItem(sys.maxint, str(x['id']))
			for y in range(len(structure)):
				self._elements['table'].SetStringItem(index, y, str(x[structure[y]]))


class Ui_form():
	def __init__(self, panel, parent, fit = wx.CENTER):
		self._panels, self._sizers, self._elements, self._parent = {}, {}, {}, parent;
		self._panels['root'] = panel
		self._elements['grid'] = wx.GridSizer(rows = 4, cols = 2, hgap = 5, vgap = 5)
		self._sizers['fieldset'] = wx.BoxSizer(wx.VERTICAL)
		self._sizers['fieldset'].Add(self._elements['grid'], 0, fit, 50)
		self._sizers['fieldset'].Add((20,20), 0) # this is a spacer
		self._elements['submit'] = wx.Button(self._panels['root'], wx.ID_ANY, "Guardar")
		self._elements['submit'].SetBackgroundColour(CONSTANTS['palete']['normal'])
		self._elements['submit'].SetForegroundColour(CONSTANTS['palete']['dark'])
		self._sizers['fieldset'].Add(self._elements['submit'], 0, fit, 5)

		self._panels['root'].SetSizer(self._sizers['fieldset'])

		self._render()

	def _render(self):
		return True

	def field_as_warning(self, field):
		self._elements[field].SetBackgroundColour(CONSTANTS['palete']['red'])
		self._elements[field].SetForegroundColour(CONSTANTS['palete']['white'])

	def field_as_normal(self, field):
		self._elements[field].SetBackgroundColour(CONSTANTS['palete']['white'])
		self._elements[field].SetForegroundColour(CONSTANTS['palete']['black'])

	def as_warning(self):
		self._elements['submit'].SetBackgroundColour(CONSTANTS['palete']['red'])
		self._elements['submit'].SetForegroundColour(CONSTANTS['palete']['white'])

	def as_normal(self):
		self._elements['submit'].SetBackgroundColour(CONSTANTS['palete']['normal'])
		self._elements['submit'].SetForegroundColour(CONSTANTS['palete']['dark'])

	def _insert_field_text_group(self, field, label, readonly = False):
		_tmp_sizer = wx.BoxSizer(wx.HORIZONTAL)
		_tmp_element_label = wx.StaticText(self._panels['root'], wx.ID_ANY, label)
		if readonly:
			self._elements['field_' + field] = wx.TextCtrl(self._panels['root'], wx.ID_ANY, style=wx.TE_READONLY)
		else:
			self._elements['field_' + field] = wx.TextCtrl(self._panels['root'], wx.ID_ANY)
		_tmp_sizer.Add((20,20), 1, wx.EXPAND) # this is a spacer
		_tmp_sizer.Add(_tmp_element_label, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)

		self._elements['grid'].Add(_tmp_sizer, 0, wx.ALIGN_LEFT)
		self._elements['grid'].Add(self._elements['field_' + field], 0, wx.EXPAND)

	def _insert_field_textarea_group(self, field, label):
		_tmp_sizer = wx.BoxSizer(wx.HORIZONTAL)
		_tmp_element_label = wx.StaticText(self._panels['root'], wx.ID_ANY, label)
		self._elements['field_' + field] = wx.TextCtrl(self._panels['root'], wx.ID_ANY, style = wx.TE_MULTILINE)
		_tmp_sizer.Add((20,20), 1, wx.EXPAND) # this is a spacer
		_tmp_sizer.Add(_tmp_element_label, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)

		self._elements['grid'].Add(_tmp_sizer, 0, wx.ALIGN_LEFT)
		self._elements['grid'].Add(self._elements['field_' + field], 0, wx.EXPAND)

	def _insert_field_check_group(self, field, label):
		_tmp_sizer = wx.BoxSizer(wx.HORIZONTAL)
		_tmp_element_label = wx.StaticText(self._panels['root'], wx.ID_ANY, label)
		self._elements['field_' + field] = wx.CheckBox(self._panels['root'], label= label)
		self._elements['field_' + field].SetBackgroundColour(CONSTANTS['palete']['normal'])
		_tmp_sizer.Add((20,20), 1, wx.EXPAND) # this is a spacer
		_tmp_sizer.Add(_tmp_element_label, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)

		self._elements['grid'].Add(_tmp_sizer, 0, wx.ALIGN_LEFT)
		self._elements['grid'].Add(self._elements['field_' + field], 0, wx.EXPAND)

		
	def _insert_field_select_group(self, field, label, data):
		_tmp_sizer = wx.BoxSizer(wx.HORIZONTAL)
		_tmp_element_label = wx.StaticText(self._panels['root'], wx.ID_ANY, label)
		self._elements['field_' + field] = wx.ComboBox(self._panels['root'], -1, pos=(50, 170), size=(150, -1), choices = data, style = wx.CB_READONLY)

		_tmp_sizer.Add((20,20), 1, wx.EXPAND) # this is a spacer
		_tmp_sizer.Add(_tmp_element_label, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)

		self._elements['grid'].Add(_tmp_sizer, 0, wx.ALIGN_LEFT)
		self._elements['grid'].Add(self._elements['field_' + field], 0, wx.EXPAND)

	def _set_text_value(self, field, value):
		self._elements['field_' + field].SetValue(value)

	def _set_check_value(self, field, value):
		self._elements['field_' + field].SetValue(value)

	def _set_boolean_value(self, field, value):
		#self._elements['field_' + field].SetValue(value)
		print "boolean"