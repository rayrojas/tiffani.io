from settings import CONSTANTS

import requests

class DataSet:
	def __init__(self, id, text):
		self.id = id
		self.text = text

	def __unicode__(self):
		return self.text

class Rest:
	def __init__(self, uri, params):
		self.uri = uri
		self.params = params
		
	def get(self):
		q = requests.get('%s%s?format=json' % (CONSTANTS['source'], self.uri), params = self.params )
		if q.status_code == 200:
			if q.json()['meta']['total_count'] > 0:
				return q.json()['objects']
			else:
				return []
		else:
			print q.text
			return []