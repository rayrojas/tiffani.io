#encoding:utf-8
import wx
import sys
from wx.lib.mixins.listctrl import CheckListCtrlMixin, ListCtrlAutoWidthMixin

from core.ui import Ui, Ui_form, Ui_table

from core.ui.settings import CONSTANTS

import requests

class Ui_supplier(Ui):
	def _render(self):
		self._constants = {
			'title' : 'Proveedores',
			'icon' : 'tiffani.io.supplier.png'
		}
		self._render_head()
		#self._create_sizer('sizer_search', wx.VERTICAL)
		#self._create_text('head', 'label-search', 'Proveedores')
		#self._insert_element_to_sizer('label-search', 'sizer_search', style = wx.EXPAND)
		#self._insert_sizer_to_panel('sizer_search', 'head')

		self._render_split_line()
		#self._render_split()

		self._render_content()
		self._create_sizer('s-c-wrapper', wx.HORIZONTAL)	#	sizer-content-wrapper

		##LEFT
		self._create_panel('content', 'p-c-left')	#	panel-content-left
		self._create_sizer('s-c-w-left', wx.VERTICAL)	#	
		self._create_panel('p-c-left', 'p-c-l-text')	#	panel-content-left-text
		self._create_panel('p-c-left', 'p-c-l-table')	#	panel-content-left-table
		#self._create_sizer('s-c-w-l-table', wx.VERTICAL)

		self._insert_panel_to_sizer('p-c-l-text', 's-c-w-left', type = "normal", style = wx.EXPAND)
		self._insert_panel_to_sizer('p-c-l-table', 's-c-w-left', type = "expand", style = wx.ALL|wx.EXPAND, border = 5)

		self._faster_create_text('p-c-l-text', 'Directorio')

		self._elements['p-c-l-table-element-table'] = Ui_table(self._panels['p-c-l-table'], self._parent, style = "expand")
		self._elements['p-c-l-table-element-table']._set_columns([
			{'position' : 0, 'name': 'Nombre', 'width' : 150},
			{'position' : 0, 'name': 'Codigo', 'width' : 70},
		])

		self._elements['p-c-l-table-element-table']._elements['table'].Bind(wx.EVT_LIST_ITEM_SELECTED, self.OnClickRow)

		uri = '%sr/supplier/?format=json' % CONSTANTS['source']
		query = requests.get(uri, params = {} )
		if query.status_code == 200:
			self._elements['p-c-l-table-element-table']._set_data(query.json()['objects'])



		self._insert_sizer_to_panel('s-c-w-left', 'p-c-left')

		self._insert_panel_to_sizer('p-c-left', 's-c-wrapper',type = "expand", style = wx.EXPAND)


		##CENTER
		self._create_panel('content', 'p-c-center')	#	panel-content-right
		self._create_sizer('s-c-w-center', wx.VERTICAL)	#
		self._create_panel('p-c-center', 'p-c-c-line')	#	panel-content-right-text
		#self._panels['p-c-c-line'].SetBackgroundColour()

		self._insert_panel_to_sizer('p-c-c-line', 's-c-w-center', type = "expand", style = wx.ALL|wx.EXPAND, border = 5)

		self._faster_create_line('p-c-c-line', style = "expand")

		self._insert_sizer_to_panel('s-c-w-center', 'p-c-center')
		
		self._insert_panel_to_sizer('p-c-center', 's-c-wrapper',type = "normal", style = wx.EXPAND)

		##RIGHT
		self._create_panel('content', 'p-c-right')	#	panel-content-right
		self._create_sizer('s-c-w-right', wx.VERTICAL)	#	
		self._create_panel('p-c-right', 'p-c-r-text')	#	panel-content-right-text
		self._create_panel('p-c-right', 'p-c-r-form')	#	panel-content-right-table		

		self._insert_panel_to_sizer('p-c-r-text', 's-c-w-right', type = "normal", style = wx.EXPAND)
		self._insert_panel_to_sizer('p-c-r-form', 's-c-w-right', type = "expand", style = wx.ALL|wx.EXPAND, border = 5)

		self._faster_create_text('p-c-r-text', 'Registrar Proveedor')

		self._elements['form'] = Ui_form(self._panels['p-c-r-form'], self, wx.EXPAND)
		self._elements['form']._insert_field_text_group('id', 'ID')
		self._elements['form']._insert_field_text_group('name', 'Nombre')
		self._elements['form']._insert_field_check_group('status', 'Estado')
		#form._insert_field_text_group('nombre', 'Ingrese nombre')


		self._insert_sizer_to_panel('s-c-w-right', 'p-c-right')

		self._insert_panel_to_sizer('p-c-right', 's-c-wrapper',type = "expand", style = wx.EXPAND)

		self._insert_sizer_to_panel('s-c-wrapper', 'content')
		self._render_end()

	def OnClickRow(self, event):
		self.GetRow(self._elements['p-c-l-table-element-table']._elements['table'].GetItem(event.m_itemIndex, 0).GetText())

	def GetRow(self, id):
		uri = '%sr/supplier/?format=json' % CONSTANTS['source']
		query = requests.get(uri, params = {'id' : id} )
		if query.status_code == 200:
			if query.json()['meta']['total_count'] > 0:
				self._elements['form']._set_text_value('id', str(query.json()['objects'][0]['id']))
				self._elements['form']._set_text_value('name', query.json()['objects'][0]['name'])