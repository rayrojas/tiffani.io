#encoding:utf-8
import wx
import sys
import json
from datetime import datetime

from wx.lib.mixins.listctrl import CheckListCtrlMixin, ListCtrlAutoWidthMixin

from core.ui import Ui, Ui_form, Ui_table

from core.ui.data import DataSet, Rest

from core.ui.settings import CONSTANTS


class Ui_transactions(Ui):
	def _render(self):
		self._constants = {
			'title' : 'Transacciones',
			'icon' : 'tiffani.io.transaction.png',
		}
		self._render_head()
		self._render_split_line()
		self._render_content()

		self._create_sizer('s-c-wrapper', wx.VERTICAL)

		#########
		##	Creando el panel top
		#########

		self._create_panel('content', 'p-c-top')

		self._create_sizer('s-c-w-top', wx.VERTICAL)
		
		self._insert_sizer_to_panel('s-c-w-top', 'p-c-top')

		r = Rest('r/item/', {'id' : self._data['item']})
		objects = r.get()
		if objects:
			self._create_static_box('p-c-top', 'sb-c-top', 'Transacciones de %s' % objects[0]['name']) # Genera s-name ...
		else:
			return False

		self._insert_panel_to_sizer('p-c-top', 's-c-wrapper',type = "expand", style = wx.EXPAND|wx.ALL, border = 5)

		self._create_panel('p-c-top', 'p-c-t-table')

		self._elements['p-c-t-table-element-table'] = Ui_table(self._panels['p-c-t-table'], self._parent, style = "expand")
		self._elements['p-c-t-table-element-table']._set_columns([
			{'position' : 1, 'name': 'ID', 'width' : 150},
			{'position' : 2, 'name': 'Codigo', 'width' : 150},
			{'position' : 3, 'name': 'Item', 'width' : 150},
			{'position' : 4, 'name': 'Tipo', 'width' : 150},
			{'position' : 5, 'name': 'Proveedor', 'width' : 150},
			{'position' : 6, 'name': 'Transaccion', 'width' : 150},
			{'position' : 7, 'name': 'Cantidad', 'width' : 150},

		])

		self.getData()

		self._elements['s-sb-c-top'].Add(self._panels['p-c-t-table'], 1, wx.EXPAND|wx.ALL, border = 10)

		#self._elements['p-c-t-table-element-table']._elements['table'].Bind(wx.EVT_CONTEXT_MENU, self.OnContextMenu)
		#self._elements['p-c-t-table-element-table']._elements['table'].Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.OnClickRightRow)

		############################################################################################################
		#	Insertando todo en el contenedor
		############################################################################################################

		self._insert_sizer_to_panel('s-c-wrapper', 'content')

		self._render_end()

	def getData(self):
		r = Rest('r/kardex/', {'item' : self._data['item']})
		objects = r.get()
		self._elements['p-c-t-table-element-table']._set_data(['id', 'item_code_text', 'item_text', 'item_kind_text', 'item_supplier_text', 'types_text', 'amount'], objects)