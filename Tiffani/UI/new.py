#encoding:utf-8
import wx
import sys

from core.ui import Ui, Ui_form

from core.ui.settings import CONSTANTS

import requests

class Ui_new(Ui):
	def _render(self):
		self._constants = {
			'title' : 'Registro de Inventario',
			'icon' : 'tiffani.io.new.png'
		}
		self._render_head()
		'''
		self._create_sizer('sizer_search', wx.VERTICAL)
		self._create_text('head', 'label-search', 'Registro de un material')
		self._insert_element_to_sizer('label-search', 'sizer_search', style = wx.EXPAND)
		self._insert_sizer_to_panel('sizer_search', 'head')
		'''
		self._render_split()

		self._render_content()

		form = Ui_form(self._panels['content'], self)
	
		uri = '%sr/kind/?format=json' % CONSTANTS['source']
		query = requests.get(uri, params = {} )
		if query.status_code == 200:
			data = []
			for x in query.json()['objects']:
				data.append(x['name'])
			form._insert_field_select_group('tipo', 'Tipo:', data)

		uri = '%sr/material/?format=json' % CONSTANTS['source']
		query = requests.get(uri, params = {} )
		if query.status_code == 200:
			data = []
			for x in query.json()['objects']:
				data.append(x['name'])
			form._insert_field_select_group('composicion', 'Tipo de composicion', data)

		uri = '%sr/supplier/?format=json' % CONSTANTS['source']
		query = requests.get(uri, params = {} )
		if query.status_code == 200:
			data = []
			for x in query.json()['objects']:
				data.append(x['name'])
			form._insert_field_select_group('proveedor', 'Proveedor:', data)

		form._insert_field_text_group('nombre', 'Ingrese nombre')

		self._render_end()
