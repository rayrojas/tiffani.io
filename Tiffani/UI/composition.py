#encoding:utf-8
import wx
import sys
import json

from wx.lib.mixins.listctrl import CheckListCtrlMixin, ListCtrlAutoWidthMixin

from core.ui import Ui, Ui_form, Ui_table

from core.ui.settings import CONSTANTS

import requests

class Ui_composition(Ui):
	def _render(self):
		self._constants = {
			'title' : 'Composiciones',
			'icon' : 'tiffani.io.composition.png',
			'layout' : [["Lista de Composiciones","Nueva Composición"],]
		}
		self._render_head()
		self._render_split_line()
		self._render_content()

		self._create_sizer('s-c-wrapper', wx.HORIZONTAL)
		#########
		##	Creando el panel izquierdo
		#########
		self._create_panel('content', 'p-c-left')

		self._create_sizer('s-c-w-left', wx.VERTICAL)
		
		self._insert_sizer_to_panel('s-c-w-left', 'p-c-left')

		self._create_static_box('p-c-left', 'sb-c-left', 'Lista de Composiciones') # Genera s-name ...


		self._insert_panel_to_sizer('p-c-left', 's-c-wrapper',type = "expand", style = wx.EXPAND|wx.ALL, border = 5)

		self._create_panel('p-c-left', 'p-c-l-table')

		self._elements['p-c-l-table-element-table'] = Ui_table(self._panels['p-c-l-table'], self._parent, style = "expand")
		self._elements['p-c-l-table-element-table']._set_columns([
			{'position' : 0, 'name': 'Nombre', 'width' : 150},
			{'position' : 0, 'name': 'Codigo', 'width' : 70},
			{'position' : 0, 'name': 'Id', 'width' : 70},
		])

		self.getData()

		self._elements['s-sb-c-left'].Add(self._panels['p-c-l-table'], 1, wx.EXPAND|wx.ALL, border = 10)

		self._elements['p-c-l-table-element-table']._elements['table'].Bind(wx.EVT_LIST_ITEM_SELECTED, self.OnClickRow)

		#########
		##	Creando el panel derecho
		#########

		self._create_panel('content', 'p-c-right')	#	panel-content-right

		self._create_sizer('s-c-w-right', wx.VERTICAL)	#	

		self._insert_sizer_to_panel('s-c-w-right', 'p-c-right')

		self._create_static_box('p-c-right', 'sb-c-right', 'Nueva Composición') # Genera s-name ...

		self._insert_panel_to_sizer('p-c-right', 's-c-wrapper',type = "expand", style = wx.EXPAND|wx.ALL, border = 5)

		self._create_panel('p-c-right', 'p-c-r-form')

		self._elements['form'] = Ui_form(self._panels['p-c-r-form'], self, wx.EXPAND)
		self._elements['form']._insert_field_text_group('id', 'ID')
		self._elements['form']._insert_field_text_group('code', 'Codigo')
		self._elements['form']._insert_field_text_group('name', 'Nombre')
		self._elements['form']._insert_field_check_group('status', 'Estado')

		self._elements['s-sb-c-right'].Add(self._panels['p-c-r-form'], 0, wx.EXPAND|wx.ALL, border = 10)

		############################################################################################################
		#	Insertando todo en el contenedor
		############################################################################################################

		self._insert_sizer_to_panel('s-c-wrapper', 'content')

		self._render_end()

		self._elements['form']._elements['submit'].Bind(wx.EVT_BUTTON, self.OnSubmit)

	def OnSubmit(self, event):
		#uri = '%sget/token/' % CONSTANTS['source']
		#query = requests.get(uri)
		#if query.status_code == 200:
		#	#####
		uri = '%scomposition/post/' % CONSTANTS['source']
		print uri
		query = requests.post(uri, data = {'code' : 'coigo', 'name' : 'aaaaa', 'staff' : 2, 'status' : True})
		print query.status_code
		if query.status_code == 200:
			self._elements['p-c-l-table-element-table']._elements['table'].DeleteAllItems()
			self.getData()
		else:
			print "error"

	def OnClickRow(self, event):
		self.GetRow(self._elements['p-c-l-table-element-table']._elements['table'].GetItem(event.m_itemIndex, 0).GetText())
		self._elements['form'].as_warning()
		self._elements['form']._elements['submit'].SetLabel('Guardar Edición')

	def getData(self):
		uri = '%sr/composition/?format=json' % CONSTANTS['source']
		query = requests.get(uri, params = {} )
		if query.status_code == 200:
			self._elements['p-c-l-table-element-table']._set_data(['id','code','name'], query.json()['objects'])

	def GetRow(self, id):
		uri = '%sr/composition/?format=json' % CONSTANTS['source']
		query = requests.get(uri, params = {'id' : id} )
		if query.status_code == 200:
			if query.json()['meta']['total_count'] > 0:
				self._elements['form']._set_text_value('id', str(query.json()['objects'][0]['id']))
				self._elements['form']._set_text_value('code', query.json()['objects'][0]['code'])
				self._elements['form']._set_text_value('name', query.json()['objects'][0]['name'])
				if query.json()['objects'][0]['status']:
					self._elements['form']._set_check_value('status', True)
				else:
					self._elements['form']._set_check_value('status', False)