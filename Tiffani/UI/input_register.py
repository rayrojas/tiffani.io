#encoding:utf-8
import wx
import sys
import json
from datetime import datetime

from wx.lib.mixins.listctrl import CheckListCtrlMixin, ListCtrlAutoWidthMixin

from core.ui import Ui, Ui_form, Ui_table

from core.ui.data import DataSet

from core.ui.settings import CONSTANTS

import requests

class Ui_input_register(Ui):
	def _render(self):
		self._constants = {
			'title' : 'Registrar Entrada',
			'icon' : 'tiffani.io.transaction.png',
		}
		self._render_head()
		self._render_split_line()
		self._render_content()

		self._create_sizer('s-c-wrapper', wx.HORIZONTAL)

		#########
		##	Creando el panel derecho
		#########

		self._create_panel('content', 'p-c-left')	#	panel-content-left

		self._create_sizer('s-c-w-left', wx.VERTICAL)	#	

		self._insert_sizer_to_panel('s-c-w-left', 'p-c-left')

		self._create_static_box('p-c-left', 'sb-c-left', 'Formulario') # Genera s-name ...

		self._insert_panel_to_sizer('p-c-left', 's-c-wrapper',type = "expand", style = wx.EXPAND|wx.ALL, border = 5)

		self._create_panel('p-c-left', 'p-c-l-form')

		self._elements['form'] = Ui_form(self._panels['p-c-l-form'], self, wx.EXPAND)
		self._elements['form']._insert_field_text_group('item', 'Item', True)

		uri = '%sr/item/' % CONSTANTS['source']
		query = requests.get(uri, params = {'id' : self._data['item']})
		if query.status_code == 200:
			if len(query.json()['objects']):
				self._elements['form']._elements['field_item'].SetValue(query.json()['objects'][0]['name'])

		self._elements['form']._insert_field_select_group('tipo_persona', 'Tipo Persona', ['Tejedora', 'Otro'])
		self._elements['form']._elements['field_tipo_persona'].Bind(wx.EVT_COMBOBOX, self.OnSelect_tipopersona)
		


		self._elements['form']._insert_field_select_group('persona', 'Persona', ['Tejedora', 'Otro'])
		self._elements['form']._insert_field_text_group('fecha', 'Fecha', True)

		self._elements['form']._elements['field_fecha'].SetValue(datetime.now().strftime('%d/%m/%Y'))

		self._elements['form']._insert_field_text_group('cantidad', 'Cantidad ( gramos )')
		self._elements['s-sb-c-left'].Add(self._panels['p-c-l-form'], 0, wx.EXPAND|wx.ALL, border = 10)

		############################################################################################################
		#	Insertando todo en el contenedor
		############################################################################################################

		self._insert_sizer_to_panel('s-c-wrapper', 'content')

		self._render_end()

		self._elements['form']._elements['submit'].Bind(wx.EVT_BUTTON, self.OnSubmit)
		self._elements['form']._elements['field_cantidad'].Bind(wx.EVT_TEXT, self.OnChange_cantidad)

	def OnChange_cantidad(self, event):
		try:
			cantidad = int(event.GetString())
			self._elements['form'].field_as_normal('field_cantidad')
		except Exception, e:
			self._elements['form'].field_as_warning('field_cantidad')


	def OnSelect_tipopersona(self, event):
		self._elements['form']._elements['field_persona'].Clear()
		if event.GetString() == 'Tejedora':
			uri = '%sr/staff/' % CONSTANTS['source']
			query = requests.get(uri, params = {'groups' : '2'})
			if query.status_code == 200:
				for x in query.json()['objects']:
					data = DataSet(x['id'], x['first_name'] + " " + x['last_name'])
					self._elements['form']._elements['field_persona'].Append(data.text, data)
			else:
				print query.text

		elif event.GetString() == 'Otro':
			print "a"

	def OnSubmit(self, event):
		persona = self._elements['form']._elements['field_persona'].GetClientData(self._elements['form']._elements['field_persona'].GetSelection())
		try:
			self._data['submit__persona'] = persona.id
			self._data['submit__cantidad'] = self._elements['form']._elements['field_cantidad'].GetValue()
			self._data['submit__item'] = self._data['item']
			self._data['submit__fecha'] = self._elements['form']._elements['field_fecha'].GetValue()
		except Exception, e:
			self._elements['form'].as_warning()
			return False
		
		self._elements['form'].as_normal()
		
		uri = '%skardex/post/insert/' % CONSTANTS['source']
		query = requests.post(uri, data = {'item' : self._data['submit__item'],
											'amount' : self._data['submit__cantidad'],
											'types' : 'INP',
											'description' : '---',
											'status' : True,
											'staff' : 2})
		if query.status_code == 200:
			self._parent.show_panel_transactions({'item' : self._data['submit__item']})
		else:
			print "error"		

	def OnClickRow(self, event):
		self.GetRow(self._elements['p-c-l-table-element-table']._elements['table'].GetItem(event.m_itemIndex, 0).GetText())
		self._elements['form'].as_warning()
		self._elements['form']._elements['submit'].SetLabel('Guardar Edición')

	def getData(self):
		uri = '%sr/composition/?format=json' % CONSTANTS['source']
		query = requests.get(uri, params = {} )
		if query.status_code == 200:
			self._elements['p-c-l-table-element-table']._set_data(['id','code','name'], query.json()['objects'])

	def GetRow(self, id):
		uri = '%sr/composition/?format=json' % CONSTANTS['source']
		query = requests.get(uri, params = {'id' : id} )
		if query.status_code == 200:
			if query.json()['meta']['total_count'] > 0:
				self._elements['form']._set_text_value('id', str(query.json()['objects'][0]['id']))
				self._elements['form']._set_text_value('code', query.json()['objects'][0]['code'])
				self._elements['form']._set_text_value('name', query.json()['objects'][0]['name'])
				if query.json()['objects'][0]['status']:
					self._elements['form']._set_check_value('status', True)
				else:
					self._elements['form']._set_check_value('status', False)