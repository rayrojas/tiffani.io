#encoding:utf-8
import wx
import sys
import json
from datetime import datetime

from wx.lib.mixins.listctrl import CheckListCtrlMixin, ListCtrlAutoWidthMixin

from core.ui import Ui, Ui_form, Ui_table

from core.ui.data import DataSet, Rest

from core.ui.settings import CONSTANTS

import requests

class Ui_lote(Ui):
	def _render(self):
		self._constants = {
			'title' : 'Lote',
			'icon' : 'tiffani.io.transaction.png',
		}
		self._render_head()
		self._render_split_line()
		self._render_prepare_layout()
		#self._render_content_layout(wx.HORIZONTAL, ['Lotes de este producto', ['Totales', 'Transacciones']]) # Genera s-c-wrapper
		self._render_content_layout([
										{
											'parent' : 'content',
											's_z' : 's-c-wrapper',
											'layout' : {
												'text' : 'Lotes del producto', 'orient' : wx.HORIZONTAL,
											}
										},
										{
											'parent' : 'content',
											's_z' : 's-c-wrapper',
											'orient' : 'VERTICAL',
											'layout' : [
												{'text' : 'Totales en este Lote', 'orient' : wx.HORIZONTAL},
												{'text' : 'Transacciones del Lote', 'orient' : wx.HORIZONTAL},
											]
										},
									], False)
		
		#########################################################################################################
		#	PANELES
		#	content-p-c-l-0
		#	p-c-l-1-t-p-c-l-0
		#	p-c-l-1-t-p-c-l-0
		########################
		#	STATICBOXSIZERS
		#	content-s-sb-c-l-0
		#	p-c-l-1-t-s-sb-c-l-0
		#	p-c-l-1-t-s-sb-c-l-0
		#########################################################################################################



		############################################################################################################
		#	Trabajando en el primer panel del layout
		############################################################################################################
		
		self._create_panel('content-p-c-l-0', 'p-c-t-table')
		self._elements['p-c-t-table-element-table'] = Ui_table(self._panels['p-c-t-table'], self._parent, style = "expand")
		self._elements['p-c-t-table-element-table']._set_columns([
			{'position' : 1, 'name': 'ID', 'width' : 50},
			{'position' : 2, 'name': 'Codigo', 'width' : 80},
			{'position' : 3, 'name': 'Tipo', 'width' : 80},
			{'position' : 4, 'name': 'Fecha', 'width' : 80},
		])

		self._elements['content-s-sb-c-l-0'].Add(self._panels['p-c-t-table'], 1, wx.EXPAND|wx.ALL, border = 10)
		
		############################################################################################################
		#	Trabajando en el segundo panel del layout
		############################################################################################################
		#self._create_panel('p-c-l-0', 'p-c-t-table')

		############################################################################################################
		#	Insertando todo en el contenedor
		############################################################################################################

		self._insert_sizer_to_panel('s-c-wrapper', 'content')

		self._render_end()

		self.getData()

		self._elements['p-c-t-table-element-table']._elements['table'].Bind(wx.EVT_LIST_ITEM_SELECTED, self.OnClickRow)

	def OnClickRow(self, event):
		print self._elements['p-c-t-table-element-table']._elements['table'].GetItem(event.m_itemIndex, 0).GetText()

	def getData(self):
		r = Rest('rl/kardex/', {'item' : self._data['item'], 'lote__classname' : 'lote'})
		objects = r.get()
		data = []
		for x in objects:
			data.append(x['lote'])
		self._elements['p-c-t-table-element-table']._set_data(['id', 'code', 'types_text', 'creation_text'], data)



		#self._elements['form']._elements['submit'].Bind(wx.EVT_BUTTON, self.OnSubmit)
		#self._elements['form']._elements['field_cantidad'].Bind(wx.EVT_TEXT, self.OnChange_cantidad)
