#encoding:utf-8
import wx
import sys
import json

from wx.lib.mixins.listctrl import CheckListCtrlMixin, ListCtrlAutoWidthMixin

from core.ui import Ui, Ui_form, Ui_table

from core.ui.settings import CONSTANTS

import requests

class Ui_search(Ui):
	def _render(self):
		self._constants = {
			'title' : 'Búsqueda',
			'icon' : 'tiffani.io.search.png',
		}
		self._render_head()
		self._render_split_line()
		self._render_content()

		self._create_sizer('s-c-wrapper', wx.VERTICAL)

		#########
		##	Creando el panel top
		#########

		self._create_panel('content', 'p-c-top')	#	paneb-content-top

		self._create_sizer('s-c-w-top', wx.VERTICAL)	#	

		self._insert_sizer_to_panel('s-c-w-top', 'p-c-top')

		self._create_static_box('p-c-top', 'sb-c-top', 'Búsqueda de inventario') # Genera s-name ...

		self._insert_panel_to_sizer('p-c-top', 's-c-wrapper',type = "normal", style = wx.EXPAND|wx.ALL, border = 5)

		self._create_panel('p-c-top', 'p-c-t-form')

		self._create_input_text('p-c-t-form', 'input-search')

		self._create_sizer('sizer_search', wx.VERTICAL)

		self._insert_element_to_sizer('input-search', 'sizer_search', style = wx.EXPAND)

		self._insert_sizer_to_panel('sizer_search', 'p-c-t-form')

		#self._elements['form'] = Ui_form(self._panels['p-c-t-form'], self, wx.EXPAND)
		#self._elements['form']._insert_field_text_group('id', 'ID')

		self._elements['s-sb-c-top'].Add(self._panels['p-c-t-form'], 0, wx.EXPAND|wx.ALL, border = 10)

		#########
		##	Creando el panel top
		#########
		self._create_panel('content', 'p-c-bottom')

		self._create_sizer('s-c-w-bottom', wx.VERTICAL)
		
		self._insert_sizer_to_panel('s-c-w-bottom', 'p-c-bottom')

		self._create_static_box('p-c-bottom', 'sb-c-bottom', 'Lista de Inventario') # Genera s-name ...


		self._insert_panel_to_sizer('p-c-bottom', 's-c-wrapper',type = "expand", style = wx.EXPAND|wx.ALL, border = 5)

		self._create_panel('p-c-bottom', 'p-c-b-table')

		self._elements['p-c-b-table-element-table'] = Ui_table(self._panels['p-c-b-table'], self._parent, style = "expand")
		self._elements['p-c-b-table-element-table']._set_columns([
			{'position' : 1, 'name': 'Id', 'width' : 70},
			{'position' : 2, 'name': 'Codigo', 'width' : 70},
			{'position' : 3, 'name': 'Tipo', 'width' : 150},
			{'position' : 4, 'name': 'Grosor', 'width' : 150},
			{'position' : 5, 'name': 'Color', 'width' : 150},
			{'position' : 6, 'name': 'Composicion', 'width' : 150},
			{'position' : 7, 'name': 'Proveedor', 'width' : 150},
			{'position' : 8, 'name': 'Total', 'width' : 150},
		])

		self.getData()

		self._elements['s-sb-c-bottom'].Add(self._panels['p-c-b-table'], 1, wx.EXPAND|wx.ALL, border = 10)

		#self._elements['p-c-b-table-element-table']._elements['table'].Bind(wx.EVT_LIST_ITEM_SELECTED, self.OnClickRow)
		self._elements['p-c-b-table-element-table']._elements['table'].Bind(wx.EVT_CONTEXT_MENU, self.OnContextMenu)
		self._elements['p-c-b-table-element-table']._elements['table'].Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.OnClickRightRow)

		############################################################################################################
		#	Insertando todo en el contenedor
		############################################################################################################

		self._insert_sizer_to_panel('s-c-wrapper', 'content')

		self._render_end()

		self._elements['input-search'].Bind(wx.EVT_KEY_UP, self.event_key_up_input_search)

	def OnClickRightRow(self, event):
		self._data['item_selected'] = self._elements['p-c-b-table-element-table']._elements['table'].GetItem(event.m_itemIndex, 0).GetText()

	def OnContextMenu(self, event):
		self._elements['context_menu'] = wx.Menu()
		self._parent.Bind(wx.EVT_MENU, self.OnSeeTransaccions, self._elements['context_menu'].Append(wx.ID_ANY, "Ver Transacciones"))
		self._parent.Bind(wx.EVT_MENU, self.OnOutput_register, self._elements['context_menu'].Append(wx.ID_ANY, "Registrar Salida"))
		self._parent.Bind(wx.EVT_MENU, self.OnInput_register, self._elements['context_menu'].Append(wx.ID_ANY, "Registrar Entrada"))
		self._parent.Bind(wx.EVT_MENU, self.Onrefresh, self._elements['context_menu'].Append(wx.ID_ANY, "Actualizar / Mostrar todo"))
		self._parent.PopupMenu(self._elements['context_menu'])

	def Onrefresh(self, event):
		self._elements['p-c-b-table-element-table']._elements['table'].DeleteAllItems()
		self.getData()

	def OnSeeTransaccions(self, event):
		if 'item_selected' in self._data:
			self._parent.show_panel_transactions({'item' : self._data['item_selected']})

	def OnInput_register(self, event):
		if 'item_selected' in self._data:
			self._parent.show_panel_lote({'item' : self._data['item_selected']})

	def OnOutput_register(self, event):
		if 'item_selected' in self._data:
			self._parent.show_panel_lote({'item' : self._data['item_selected']})
			#self._parent.show_panel_output_register({'item' : self._data['item_selected']})

	#----------------------------------------------------------------------
	def onExit(self, event):
		"""
		Exit program
		"""
		self.Close()

	#----------------------------------------------------------------------
	def onPopup(self, event):
		"""
		Print the label of the menu item selected
		"""
		itemId = event.GetId()
		menu = event.GetEventObject()
		menuItem = menu.FindItemById(itemId)
		print menuItem.GetLabel()

	def event_key_up_input_search(self, event):
		string = self._elements['input-search'].GetValue()
		if len(string) > 4:
			print string
			uri = '%sr/item/' % CONSTANTS['source']
			query = requests.get(uri, params = {'name__icontains' : string} )
			if query.status_code == 200:
				self.setData(query.json()['objects'])
			else:
				print query.text	

	def OnClickRow(self, event):
		print "Click row"

	def setData(self, objects):
		self._elements['p-c-b-table-element-table']._elements['table'].DeleteAllItems()
		self._elements['p-c-b-table-element-table']._set_data(['id', 'code', 'name'], objects)

	def getData(self):
		uri = '%sr/itemyarn/?format=json' % CONSTANTS['source']
		query = requests.get(uri, params = {} )
		if query.status_code == 200:
			self._elements['p-c-b-table-element-table']._set_data(['id','code','kind_text', 'weight', 'color', 'composition', 'supplier_text', 'total_text'], query.json()['objects'])

	def GetRow(self, id):
		uri = '%sr/item/?format=json' % CONSTANTS['source']
		query = requests.get(uri, params = {'id' : id} )
		if query.status_code == 200:
			if query.json()['meta']['total_count'] > 0:
				self._elements['form']._set_text_value('id', str(query.json()['objects'][0]['id']))
				self._elements['form']._set_text_value('code', query.json()['objects'][0]['code'])
				self._elements['form']._set_text_value('name', query.json()['objects'][0]['name'])
				if query.json()['objects'][0]['status']:
					self._elements['form']._set_check_value('status', True)
				else:
					self._elements['form']._set_check_value('status', False)