import wx


class Texton():
	def __init__(self, panel):

		pnl = panel

		font = wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.BOLD)
		heading = wx.StaticText(panel, label='The Central Europe', pos=(130, 15))
		heading.SetFont(font)

		wx.StaticLine(panel, pos=(25, 50), size=(300,1))

		wx.StaticText(panel, label='Slovakia', pos=(25, 80))
		wx.StaticText(panel, label='Hungary', pos=(25, 100))
		wx.StaticText(panel, label='Poland', pos=(25, 120))
		wx.StaticText(panel, label='Czech Republic', pos=(25, 140))
		wx.StaticText(panel, label='Germany', pos=(25, 160))
		wx.StaticText(panel, label='Slovenia', pos=(25, 180))
		wx.StaticText(panel, label='Austria', pos=(25, 200))
		wx.StaticText(panel, label='Switzerland', pos=(25, 220))

		wx.StaticText(panel, label='5 445 000', pos=(250, 80))
		wx.StaticText(panel, label='10 014 000', pos=(250, 100))
		wx.StaticText(panel, label='38 186 000', pos=(250, 120))
		wx.StaticText(panel, label='10 562 000', pos=(250, 140))
		wx.StaticText(panel, label='81 799 000', pos=(250, 160))
		wx.StaticText(panel, label='2 050 000', pos=(250, 180))
		wx.StaticText(panel, label='8 414 000', pos=(250, 200))
		wx.StaticText(panel, label='7 866 000', pos=(250, 220))

		wx.StaticLine(panel, pos=(25, 260), size=(300,1))

		tsum = wx.StaticText(panel, label='164 336 000', pos=(240, 280))
		sum_font = tsum.GetFont()
		sum_font.SetWeight(wx.BOLD)
		tsum.SetFont(sum_font)

		btn = wx.Button(panel, label='Close', pos=(140, 310))

		#btn.Bind(wx.EVT_BUTTON, panel.OnClose)        
		
		#pnl.SetSize((10, 380))
		#pnl.SetTitle('wx.StaticLine')
		#self.Centre()
		#self.Show(True)      
		
	def OnClose(self, e):
		
		self.Close(True)