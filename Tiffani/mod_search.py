import wx
import sys
from wx.lib.mixins.listctrl import CheckListCtrlMixin, ListCtrlAutoWidthMixin

#Constants
Constants = {
	'border' : wx.SIMPLE_BORDER
}

packages = [('abiword', '5.8M', 'base'), ('adie', '145k', 'base'),
	('airsnort', '71k', 'base'), ('ara', '717k', 'base'), ('arc', '139k', 'base'),
	('asc', '5.8M', 'base'), ('ascii', '74k', 'base'), ('ash', '74k', 'base')]

class CheckListCtrl(wx.ListCtrl, CheckListCtrlMixin, ListCtrlAutoWidthMixin):
	def __init__(self, parent):
		wx.ListCtrl.__init__(self, parent, -1, style=wx.LC_REPORT | wx.SUNKEN_BORDER)
		CheckListCtrlMixin.__init__(self)
		ListCtrlAutoWidthMixin.__init__(self)


class Ui_search():
	def __init__(self, panel):
		self._panels, self._sizers, self._elements = {}, {}, {}
		self._panels['root'] = panel
		
		#wx.TextCtrl(self, -1, '',  style=wx.TE_RIGHT)
		#Sizers
		self._sizers['main'] = wx.BoxSizer(wx.VERTICAL)
		self._sizers['head'] = wx.BoxSizer(wx.VERTICAL)
		self._sizers['content'] = wx.BoxSizer(wx.VERTICAL)

		#Panels
		self._panels['head'] = wx.Panel(self._panels['root'], -1, style = Constants['border'])
		self._panels['content'] = wx.Panel(self._panels['root'], -1, style = Constants['border'])

		self._sizers['head'].Add(self._panels['head'], 1, wx.EXPAND)
		self._sizers['content'].Add(self._panels['content'], 1, wx.EXPAND)

		self._sizers['main'].Add(self._sizers['head'], 0, wx.EXPAND)
		self._sizers['main'].Add((-1, 20))
		self._sizers['main'].Add(self._sizers['content'], 0, wx.EXPAND)

		self._panels['root'].SetSizer(self._sizers['main'])

	def OnSelectAll(self, event):
		num = self.list.GetItemCount()
		for i in range(num):
			self.list.CheckItem(i)

	def OnDeselectAll(self, event):
		num = self.list.GetItemCount()
		for i in range(num):
			self.list.CheckItem(i, False)

	def OnApply(self, event):
		num = self.list.GetItemCount()
		for i in range(num):
			if i == 0: self.log.Clear()
			if self.list.IsChecked(i):
				self.log.AppendText(self.list.GetItemText(i) + '\n')
