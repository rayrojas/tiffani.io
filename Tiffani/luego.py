import wx

import ribbon as RB

from octopus import addtoPanel


ID_CIRCLE = wx.ID_HIGHEST + 1
ID_CROSS = ID_CIRCLE + 1
ID_TRIANGLE = ID_CIRCLE + 2
ID_SQUARE = ID_CIRCLE + 3
ID_POLYGON = ID_CIRCLE + 4
ID_SELECTION_EXPAND_H = ID_CIRCLE + 5
ID_SELECTION_EXPAND_V = ID_CIRCLE + 6
ID_SELECTION_CONTRACT = ID_CIRCLE + 7
ID_PRIMARY_COLOUR = ID_CIRCLE + 8
ID_SECONDARY_COLOUR = ID_CIRCLE + 9
ID_DEFAULT_PROVIDER = ID_CIRCLE + 10
ID_AUI_PROVIDER = ID_CIRCLE + 11
ID_MSW_PROVIDER = ID_CIRCLE + 12
ID_MAIN_TOOLBAR = ID_CIRCLE + 13
ID_POSITION_TOP = ID_CIRCLE + 14
ID_POSITION_TOP_ICONS = ID_CIRCLE + 15
ID_POSITION_TOP_BOTH = ID_CIRCLE + 16
ID_POSITION_LEFT = ID_CIRCLE + 17
ID_POSITION_LEFT_LABELS = ID_CIRCLE + 18
ID_POSITION_LEFT_BOTH = ID_CIRCLE + 19


def CreateBitmap(xpm):

	bmp = wx.Bitmap("bitmaps/%s"%xpm, wx.BITMAP_TYPE_XPM)
	
	return bmp


class ColourClientData(object):

	def __init__(self, name, colour):
		
		self._name = name
		self._colour = colour


	def GetName(self):

		return self._name

	
	def GetColour(self):

		return self._colour



class RibbonFrame(wx.Frame):

	def __init__(self, parent, id=wx.ID_ANY, title="", pos=wx.DefaultPosition,
				 size=wx.DefaultSize, style=wx.DEFAULT_FRAME_STYLE):

		wx.Frame.__init__(self, parent, id, title, pos, size, style)
		
		self._ribbon = RB.RibbonBar(self, wx.ID_ANY)

		labelOne = wx.StaticText(self._ribbon, wx.ID_ANY, 'Name')

		MainSizer = wx.BoxSizer(wx.VERTICAL)
		#RibbonSizer = wx.BoxSizer(wx.VERTICAL)
		#ContentSizer = wx.BoxSizer(wx.HORIZONTAL)

		#RibbonSizer.Add(self._ribbon, 0, wx.EXPAND)
		#ContentSizer.Add(labelOne, 1, wx.EXPAND, 5)

		#MainSizer.Add(RibbonSizer, 0, wx.EXPAND)
		#MainSizer.Add(ContentSizer, 0, wx.EXPAND, 5)
		
		MainSizer.Add(self._ribbon, 0, wx.EXPAND)

		self.SetSizer(MainSizer)


if __name__ == "__main__":

	app = wx.PySimpleApp()
	frame = RibbonFrame(None, -1, "Classic Alpaca - Almacenes", size=(800, 600))
	frame.CenterOnScreen()
	frame.Show()
	app.MainLoop()